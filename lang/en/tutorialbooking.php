<?php
// This file is part of the Tutorial Booking activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English language localisation strings.
 *
 * @package    mod_tutorialbooking
 * @copyright  2012 Nottingham University
 * @author     Benjamin Ellis - benjamin.ellis@nottingham.ac.uk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['sessionerror'] = '{$a}';
// Standard strings.
$string['modulename'] = 'Tutorial Booking';
$string['modulename_help'] = 'The tutorial booking activity allows students to sign up to a single slot.

Teachers can:

* Set the names of other people who have signed up to a slot to be visible or hidden from students.
* Print registers of students signed up to slots.
* Generate a csv file of the signups.
* Manually add and remove students from slots.
* Lock and unlock the ability to sign up.
* Send a message to everyone signed up to a slot
';
$string['moduleadminname'] = 'Tutorial Booking';
$string['pluginname'] = 'Tutorial Booking';
$string['pluginadministration'] = '';
$string['modulenameplural'] = 'Tutorial Bookings';
$string['pagetitle'] = 'Tutorial Booking';
$string['pagecrumb'] = 'Time Slots';
$string['you'] = 'You';

// Index page - call made to the module's directory.
$string['indexnoid'] = 'A course id must be stipulated to view all tutorials';
$string['thereareno'] = 'There are no tutorials in this course';

// Settings admin pages.
$string['adminnumbersprompt'] = 'Select default number of attendees';
$string['adminnumbershelp'] = 'Enter the default number of attendees for tutorials - can be overridden when configuring tutorials';
$string['adminlockprompt'] = 'Select to lock tutorials by default';
$string['adminlockhelp'] = 'Select to make all tutorials locked by default';
$string['adminblockingprompt'] = 'Switch on Blocking functionality';
$string['adminblockinghelp'] = 'Switching this on will allow blocking of signups for selected users';
$string['adminiconprompt'] = 'Use Icons?';
$string['adminiconhelp'] = 'If off, all links will be in text, no icons will be shown';
$string['adminwaitingprompt'] = 'Switch on Waiting List functionality';
$string['adminwaitinghelp'] = 'Switching this on will allow waiting lists on timeslot sessions';
$string['adminserviceprompt'] = 'Mark this instance as a live Moodle service';
$string['adminservicehelp'] = 'NOTICE: Swiching this on will mean that emails/notifications will be sent to signups - when off only main admin (id=2) gets notifications';

// Tutorial instance config (settings.php).
$string['instanceheading'] = 'General Settings';
$string['instancetitle'] = 'Sign-up List Title';
$string['instancedesc'] = 'Sign-up List Notes';
$string['instancenamehelp'] = 'e.g. Tutorial 1 or Computer Labs or Fortnightly Tutorials';
$string['instancedeschelp'] = 'Information students need to know when signing up, such as duration of session.';
$string['lockedprompt'] = 'Locked';
$string['lockhelp'] = 'If locked students will not be able to signup (or unsignup) from any timeslot in this tutorial.
Locking it now will effectively freeze the signup lists in their current state.';
$string['notimeslotediting'] = 'Since this is a new tutorial, please save this page and return to create signup lists, remember to set visibility to hide this to avoid use before tutorial has been completely setup.';
$string['lockwarning'] = 'This tutorial has been locked by the convenor. You cannot signup to (or remove yourself from) any slot.';
$string['completionsignedupgroup'] = 'Require signup';
$string['completionsignedup'] = 'Students must signup to a tutorial group in this activity to complete it.';

// Strings for displaying the lock status in the settings block.
$string['locked'] = 'Unlock tutorial';
$string['unlocked'] = 'Lock tutorial';

// Tutorial list edit.
$string['linktomanagetext'] = 'Manage Signup Lists';
$string['timeslotheading'] = 'Signup List Management';
$string['sessionpagetitle'] = 'Time Slot Management';
$string['statsline'] = '({$a->places} slots, {$a->signedup} taken)';
$string['statslineblocked'] = '({$a->places} slots, {$a->signedup} taken - {$a->blocked} blocked)';
$string['newtimslotprompt'] = 'Add a Time Slot to this sign-up list';
$string['edittutorialprompt'] = 'Edit this sign-up list';
$string['deletetutorialprompt'] = 'Delete this sign-up list';
$string['exportlistprompt'] = 'Export this sign-up list';
$string['exportcsvlistprompt'] = 'Export as CSV';
$string['exportcsvlistallprompt'] = 'Export all tutorial signups on this course as CSV';

// Timeslots (signup lists) edit.
$string['editsession'] = 'Edit/Move/Copy';
$string['hidesession'] = 'Visible - Hide Time Slot';
$string['showsession'] = 'Hidden - Make Visible';
$string['copysession'] = 'Copy Time Slot';
$string['deletesession'] = 'Delete';
$string['moveupsession'] = 'Move Up';
$string['movedownsession'] = 'Move Down';
$string['deletepageheader'] = 'Confirm Delete';
$string['deletewarningtext'] = 'Are you sure you want to delete "{$a}"';
$string['newsessionheading'] = 'New Time Slot';
$string['newsessionhelp'] = 'To create a new time slot in the above sign-up list, please fill in the form below.';
$string['editsessionheading'] = 'Edit Existing Time Slot';
$string['editsessionhelp'] = 'To modify the time slot, please fill in the form below.';
$string['unblockuserprompt'] = 'Blocked Users - Click to unblock User';
$string['blockuserprompt'] = 'Signed Up - Click to block User';
$string['sessiondescriptionprompt'] = 'Date, Time & Location';
$string['sessiondescriptionhelp'] = 'e.g. 10:00am on Thursday 14th Aug in Room B35, Business School or 10:00am on Thursday 14th & 21st Aug, and 4th Sep in Room B35, Business School.';
$string['sessiondescriptionhelp2'] = 'Please ensure you have included the name of the building!<br/>
Module Convenors should make sure that they have booked the room!';
$string['after'] = 'After {$a->session}';

// Timeslot (signup list) form.
$string['spacesprompt'] = 'Max Number of Students';
$string['positionprompt'] = 'Position';
$string['positionfirst'] = 'Top of the Page';
$string['positionlast'] = 'Bottom of the Page';
$string['save'] = 'Save';
$string['confirm'] = 'Confirm';
$string['saveasnew'] = 'Save as new time slot';
$string['reset'] = 'Undo';
$string['cancel'] = 'Cancel';
$string['editspaceserror'] = 'ERROR: You cannot reduce the number of spaces ({$a->spaces}) to less that the number of signups ({$a->signedup})';

// View timeslots.
$string['attendees'] = 'Currently signed up';
$string['numbersline'] = '{$a->total} places available in total ({$a->taken} taken, {$a->left} free)';
$string['numbersline_oversubscribed'] = '{$a->total} places available in total ({$a->taken} taken, oversubscribed by {$a->left})';
$string['blockedtotal'] = 'includes {$a} blocked user/s';

// Print registers.
$string['registerprintname'] = 'Print Register (By Name)';
$string['registerprintdate'] = 'Print Register (By Signup)';
$string['registerfooter'] = 'Please sign next to your name to indicate attendance. If your name is not on the list then please do
not add it without asking first.';
$string['registerdateline'] = 'Please Enter Date of Tutorial (dd/mm/yy):&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_&nbsp;_&nbsp;&nbsp;_&nbsp;_&nbsp;&nbsp;_&nbsp; _';
$string['studentcoltitle'] = 'Student Name';
$string['attendcoltitle'] = 'Attendance';

// Notifications.
$string['emailgroupprompt'] = 'Email group';
$string['emailpagetitle'] = 'Email Group';
$string['subjecttitleprompt'] = 'Subject';
$string['messageprompt'] = 'Message';
$string['sendmessage'] = 'Send Message';
$string['liveservicemsg'] = 'Live service recognised, notification being sent to all signups';
$string['testservicemsg'] = 'Non live service - notification being sent to Admin (id=2)';
$string['backtosession'] = 'Messages sent, click here to go back to Signup List';
$string['viewmessages'] = 'View sent messages';
$string['showallmessages'] = 'Show all messages';
$string['showmymessages'] = 'Show my messages only';
$string['senttime'] = 'Sent on';
$string['sentby'] = 'Sender';
$string['sentto'] = 'Recipients';
$string['last'] = 'Last';
$string['first'] = 'First';
$string['messagessent'] = 'Messages sent';
$string['showalltutorialbookings'] = 'Tutorial booking index';

// Messaging variables used in the messaging admin page.
$string['messageprovider:reminder'] = 'Tutorial Reminder Notification';
$string['messageprovider:notify'] = 'Tutorial Notification';

// Student actions.
$string['blockedusermessage'] = 'You have been blocked from signing up to these sessions - please see convenor for reasons';
$string['signupforslot'] = 'Sign me up for this slot';
$string['removefromslot'] = 'Remove me from this slot';

// Confirm removal strings.
$string['removeuserfromslot'] = 'Remove from this slot';
$string['confirmusersignupremoval'] = 'Confirm signup removal';
$string['confirmmessage'] = 'Are you sure you wish to remove {$a->name} from {$a->timeslot}?';
$string['messagewillbesent'] = 'Message to the student being removed';
$string['removalreason'] = 'Reason for removal';
$string['remove'] = 'Confirm removal';
$string['reasonrequired'] = 'You must provide the reason you are removing the student.';
$string['removalmessagesubject'] = 'You have been removed from {$a->timeslot}';

// Add to signup list strings.
$string['addstudents'] = 'Add Students';
$string['availabletoadd'] = 'Available to add';

// Error messages.
$string['alreadysignedup'] = 'You have already signed up to a session.';
$string['useralreadysignedup'] = 'User {$a->id} is already signed up to a session.';
$string['sessionfull'] = 'No spaces left, please pick another session.';
$string['unauthorised'] = 'You do not have permission to signup.';
$string['oversubscribed'] = 'There are {$a->freeslots} places left on {$a->timeslotname}. You tried to add {$a->numbertoadd} student.';
$string['lockederror'] = 'The tutorial is locked. You may not signup at this time.';

// Capabilities.
$string['tutorialbooking:submit'] = 'Required to signup to a tutorial booking session.';
$string['tutorialbooking:viewallmessages'] = 'Required to view messages other users sent in a tutorial booking session.';
$string['tutorialbooking:exportallcoursetutorials'] = 'Required to export list for all tutorial signups in a course.';
$string['tutorialbooking:addinstance'] = 'Allows a user to add this activity to a course (not used in Moodle 2.2)';
$string['tutorialbooking:removeuser'] = 'Allows the removal of students from a signup group.';
$string['tutorialbooking:adduser'] = 'Allows the user to add students to a signup group.';
$string['tutorialbooking:oversubscribe'] = 'Allows the user to add studets to a group even if this will take it over its user limit.';
$string['tutorialbooking:editsignuplist'] = 'Allow users to add, delete and edit the signup lists.';
$string['tutorialbooking:export'] = 'Allow the user to export tutorial signups';
$string['tutorialbooking:message'] = 'Allows the user send messages to students via the tutorialbooking activity.';
$string['tutorialbooking:printregisters'] = 'Allows the user to print registers for the activity.';
$string['tutorialbooking:viewadminpage'] = 'Allows the user to see the admin page of the activity.';

// Form validation.
$string['option_spaces_low'] = 'The number of spaces must be greater than 0';
$string['option_spaces_high'] = 'The number of spaces must be less than 65536';

// Privacy option strings.
$string['privacy'] = 'Privacy';
$string['privacy_showall'] = 'Students can see all signups';
$string['privacy_showown'] = 'Students can only see their signup';
$string['yousignedup'] = 'You are signed up to this slot';

// Export strings.
$string['timeslottitle'] = 'TimeSlotTitle';
$string['idnumber'] = 'IDNumber';
$string['username'] = 'UserName';
$string['realname'] = 'RealName';
$string['coursefullname'] = 'CourseFullname';
$string['tutorialbooking'] = 'TutorialBooking';
